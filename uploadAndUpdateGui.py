#!/usr/bin/python

'''
    This script navigates through the idrac gui to upload and install an idrac image continuously
    It switches between two image paths and always starts with one image
    If you edit the file in the future, you should use the xpath webdriver method to specify the path to the element on the page
'''

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait 
import time

# Create a new instance of the Firefox driver
driver = webdriver.Firefox()
driver.implicitly_wait(60)
toggle = False
updateIteration = 0
url = r'https://10.35.0.164/'
file1 = r'C:\Users\noah.brewer\Projects\3.20-X41.d9'
file2 = r'C:\Users\noah.brewer\Projects\3.20-X40.d9'

print('file1 = ' + file1)
print('file2 = ' + file2)
print('url = ' + url)

while (True):
    # go to the iDRAC home page
    while (True):
        try:
            driver.get(url)
        except:
            continue
        break
    updateIteration = updateIteration + 1
    print("Current update iteration: " + str(updateIteration))

    #polls forever until it succeeds.
    while (True):
        try:
            username = driver.find_element_by_name("username")
    # sleep before entering keys. Finds username too quickly. 
            time.sleep(.5)
            username.send_keys("root")
        except:
            continue
        break
    password = driver.find_element_by_name("password")
    password.send_keys("calvin")
    login = driver.find_element_by_class_name("cux-button")
    login.click()
    
    #click maintenance button
    while (True):
        try:
            maintenance = driver.find_elements_by_css_selector('li.ng-scope.dropdown')
            time.sleep(.5)
            maintenance[3].click()
        except:
            continue
        break
    
    #click system update button
    while (True):
        try:
            updateButton = driver.find_elements_by_css_selector('li.ng-isolate-scope')
            time.sleep(.5)
            updateButton[2].click()
        except:
            continue
        break

    #click browse button
    while (True):
        try:
            browse = driver.find_element_by_name('fwfile')
            time.sleep(.5)
            if toggle:
                browse.send_keys(file1)
            else:
                browse.send_keys(file2)
            toggle = ~toggle
        except:
            continue
        break

    #upload file
    while (True):
        try:
            upload = driver.find_element_by_css_selector('button.btn.ng-scope.btn-primary')
            time.sleep(.5)
            upload.click()
        except:
            continue
        break
    
    #select file for upgrade
    i = 0
    while (True):
        try:
            selectBtn = driver.find_element_by_css_selector('input.ng-scope')
            time.sleep(1)
            i = i + 1
            selectBtn.click()
            print(str(i) + " seconds to upload")
        except:
            continue
        break

    while (True):
        try:
            install = driver.find_elements_by_css_selector('button.btn.btn-primary.button-clear-right.ng-scope')
            install[1].click()
        except:
            continue
        break

    while (True):
        try:
            job  = driver.find_elements_by_css_selector('button.btn.btn-primary.ng-scope')
            time.sleep(1)
            job[4].click()
        except:
            continue
        break

    t = 200
    print("waiting " +str(t)+" seconds before polling")
    time.sleep(t)
    print("Polling idrac")
