#!/usr/bin/python
'''
    To connect and disconnect RFS
'''

from selenium import webdriver
import time
import idracGuiActions

driver = webdriver.Firefox()
updateNumber = 0

#set implicit wait
driver.implicitly_wait(60)

url = "https://10.35.0.164/"
rfsurl = "10.35.155.199:/home/noah/nfs/ubuntu.iso"

idracWeb = idracGuiActions.idracWebsite(driver)

idracWeb.get(url)

idracWeb.login()
idracWeb.execute("//strong[@accesskey='c']")
idracWeb.execute("//li[@heading='Virtual Media']")
idracWeb.execute("//input[@name='Image']")
idracWeb.send_keys(rfsurl)
while(True):
    idracWeb.execute("//button[@translate='connect']")
    idracWeb.execute("//button[@ng-click='ok();']")
    idracWeb.execute("//button[@translate='disconnect']")
    idracWeb.execute("//button[@ng-click='ok();']")

