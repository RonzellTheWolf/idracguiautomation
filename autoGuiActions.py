import pyautogui
import time

def f2andclose():
    w,h = pyautogui.size()
    pyautogui.moveTo(w/2,h/2)
    time.sleep(5)
    pyautogui.click() 
    time.sleep(2)
    pyautogui.press('f1')
    time.sleep(2)
    pyautogui.moveTo(w, 0)
    pyautogui.click()
