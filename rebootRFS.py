#JIT-100482
#http://100.97.142.82/dsu.iso
#User Name: administrator
#Password: dell@1234

#for logging
import sys
#out = sys.stdout
#sys.stdout = open('C:\\Users\\noah.brewer\\Projects\\Python\\Selenium\\httpsFStest.txt','w')
#random: for different image connect times
import random
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.by import By
import time
#log date
import datetime
#To close geckodriver
import os
import pyautogui


filename = r'\httpsOSrunningVCONSOLErunning.txt'
filestub = r"C:\Users\noah.brewer\Projects\Python\Selenium"
file = open(filestub + filename, 'w')

def printNWrite(string):
    file.write(string + '\n')
    print(string)

def logout(driver):
    printNWrite("logging out...")
    driver.switch_to_default_content()
    driver.find_element_by_xpath("//frame[@src='blankLoading.html']")
    logoutFrame = driver.find_element_by_xpath("//frame[@name='globalnav']")
    driver.switch_to.frame(logoutFrame)
    c = driver.find_element_by_xpath("//span[@id='gen_logout_lbl']")
    c.click()
    driver.switch_to_default_content()
    driver.find_element_by_xpath("//span[@id='login_lbl']")
    
def connectDisconnect(iterations, driver):
    updateNumber = 0
    temp = driver.find_element_by_xpath("//frame[@src='blankLoading.html']")
    driver.switch_to.frame(temp)
    for x in range(iterations):
        global file
        file = open(filestub + filename, 'a')
        printNWrite( '\n' + str(datetime.datetime.today()))
        updateNumber = updateNumber + 1
        printNWrite("Current RFS HTTPS fileshare connect disconnect cycle: " + str(updateNumber))
        printNWrite("Sleep before start")
        time.sleep(1)

        printNWrite("Test if connect button is clickable")
        WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//span[@id='rfs_connect_btn_lbl']")))

        printNWrite("Finding image file path text box")
        filepath = driver.find_element_by_xpath("//input[@id='remoteFileshrImage']")
        printNWrite("Inputting keys")
        time.sleep(.5)
        try:
            filepath.send_keys("https://100.97.142.82/dsu.iso")
            time.sleep(.5)
        except:
            printNWrite("Could not send keys, retrying disconnect button...")
            time.sleep(10)
            element = WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//span[@id='rfs_disconnect_lbl']")))
            printNWrite("Click disconnect button")
            element.click()
            printNWrite("Test if connect button is clickable")
            WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//span[@id='rfs_connect_btn_lbl']")))

            printNWrite("Finding image file path text box")
            filepath = driver.find_element_by_xpath("//input[@id='remoteFileshrImage']")
            printNWrite("Inputting keys")
            time.sleep(.5)
            filepath.send_keys("https://100.97.142.82/dsu.iso")
            #3
            time.sleep(.5)
            printNWrite("Send key succeeded this time.")
            
        username = driver.find_element_by_xpath("//input[@id='remoteFileshrUser']") #RFS Username textblock
        username.send_keys("administrator")
        time.sleep(.5)
        password = driver.find_element_by_xpath("//input[@id='remoteFileshrPwd']") #RFS Password textblock
        password.send_keys("dell@1234")
        
        printNWrite("Check if connect button is clickable")
        connect = driver.find_element_by_xpath("//span[@id='rfs_connect_btn_lbl']")
        printNWrite("Click connect")
        connect.click()
        #give host time to mount
        #sleeptime = random.randint(1,5)
        sleeptime = 15
        printNWrite("Sleeping for " + str(sleeptime) + " before disconnecting")
        time.sleep(sleeptime)
        printNWrite("Check if disconnect button is clickable")
        try:
            element = WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//span[@id='rfs_disconnect_lbl']")))
            printNWrite("Click disconnect button")
            element.click()
        except:
            printNWrite("Disconnect click failed, trying again...")
            time.sleep(10)
            element = WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//span[@id='rfs_disconnect_lbl']")))
            printNWrite("Click disconnect button")
            element.click()
        if updateNumber < iterations:
            file.close()

def switchToAttachedMediaTab(driver):
    while(True):
        try:
            driver.switch_to_default_content()
            driver.switch_to.frame(driver.find_element_by_xpath("/html/frameset/frameset[2]/frameset[2]/frame[1]"))
            attachedmedia = driver.find_element_by_xpath("//a[@id='li_T02']")
            attachedmedia.click()
            driver.switch_to_default_content()
            driver.switch_to.frame(driver.find_element_by_xpath("//frame[@src='blankLoading.html']"))
            driver.find_element_by_xpath("//span[@id='rfs_connect_btn_lbl']")
        except:
            continue
        break
def connectToIdracHome(driver):
    while(True):
        try:
            driver.get("https://100.69.119.132/")
        except:
            time.sleep(7)
            continue
        break
def enterUserCredentials(driver):
    while(True):
        try:
            inputElement = driver.find_element_by_name("user")
            inputElement.send_keys("root")
            inputElement2 = driver.find_element_by_name("password")
            inputElement2.send_keys("calvin")
            
            login = driver.find_element_by_id("submit_lbl")
            login.click()
            time.sleep(10)
            driver.switch_to_default_content()
            driver.switch_to.frame(driver.find_element_by_xpath("//frame[@src='blankLoading.html']"))
            driver.switch_to.frame(driver.find_element_by_xpath("//iframe[@id='sysIframe']"))
            driver.find_element_by_xpath("//span[@id='main_idrac_reset_lbl']") #ensure that we made it to the main page and it is loaded
        except:
            connectToIdracHome(driver) #start over if cant find Reset iDRAC button
            time.sleep(3)
            continue
        break
        
def main():
    driver = webdriver.Firefox()
    driver.implicitly_wait(30)
    while(True):
        connectToIdracHome(driver)
        #override = driver.find_element_by_xpath("//a[@id='overridelink']")
        #override.click()

        enterUserCredentials(driver)
        #https://stackoverflow.com/questions/18924146/selenium-and-iframe-in-html?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
        #xpath ftw
        switchToAttachedMediaTab(driver)
        driver.switch_to_default_content()
        file.close()
        connectDisconnect(1, driver)

        #restarting idrac
        driver.switch_to_default_content()
        driver.switch_to.frame(driver.find_element_by_xpath("/html/frameset/frameset[2]/frameset[2]/frame[1]"))
        properties = WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//a[@id='li_T01']")))
        properties.click()
        time.sleep(20)
        driver.switch_to_default_content()
        driver.switch_to.frame(driver.find_element_by_xpath("//frame[@src='blankLoading.html']"))
        driver.switch_to.frame(driver.find_element_by_xpath("//iframe[@id='sysIframe']"))
        time.sleep(3)
        reset = driver.find_element_by_xpath("//span[@id='main_idrac_reset_lbl']")
        reset.click()
        time.sleep(1)
        #input("Press Enter to continue...")
        pyautogui.press('enter')
        #logout(driver)
        #os.system("tskill plugin-container")
        #driver.quit()
        time.sleep(300)
main()
