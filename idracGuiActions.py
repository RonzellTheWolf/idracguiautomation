#!/usr/bin/python
import time

class idracWebsite():
    
    def __init__(self, driver):
       self.driver = driver
    
    def get(self,url):
        while(True):
            try:
                self.driver.get(url)
            except Exception as e:
                continue
            break
    
    def virtualConsoleLaunch(self):
        self.execute("//strong[@accesskey='a']")
        self.execute("//button[@ng-click='onLaunchClick();']")

    def execute(self,xpath):
        while(True):
            try: 
                self.obj = self.driver.find_element_by_xpath(xpath)
                time.sleep(.5)
                self.obj.click()
            except Exception as e:
                print(e)
                continue
            break
    def send_keys(self, keys):  
        self.obj.send_keys(keys)

    def login(self):
        while(True):
            try: 
                inputElement = self.driver.find_element_by_xpath("//input[@name='username']")
                time.sleep(1)
                inputElement.send_keys("root") 
            except Exception as e:
                print(e)
                continue
            break
        while(True):
            try: 
                inputElement2 = self.driver.find_element_by_name("password")
                inputElement2.send_keys("calvin")
                self.execute("//button[@type='submit']")
            except:
                continue
            break
    def toDashboard(self):
        self.execute("//strong[@accesskey='a']")
    def toSystem(self):
        self.execute("//strong[@accesskey='s']")
    def toStorage(self):
        self.execute("//strong[@accesskey='t']")
    def toConfiguration(self):
        self.execute("//strong[@accesskey='c']")
    def toMaintenance(self):
        self.execute("//strong[@accesskey='m']")
    def toiDRACSettings(self): 
        self.execute("//strong[@accesskey='i']")
