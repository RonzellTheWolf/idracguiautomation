#!/usr/bin/python
'''
    For JIT-102055
'''

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time
#log date
import datetime
import idracGuiActions
import autoGuiActions

driver = webdriver.Firefox()
updateNumber = 0

#set implicit wait
driver.implicitly_wait(60)

num = 0

idracWeb = idracGuiActions.idracWebsite(driver)

while(True):
    num = num + 1
    print("Iteration #" + str(num))
    print(str(datetime.datetime.now()))
    
    idracWeb.get("https://10.35.0.164/")
    
    idracWeb.login()
    idracWeb.toConfiguration()
    idracWeb.execute("//li[@heading='BIOS Settings']")
    while(True):
        try: 
            miscSettings = driver.find_elements_by_xpath("//label[@ng-bind='menu.DisplayName']")
            time.sleep(.5)
            miscSettings[12].click()
        except:
            continue
        break
    time.sleep(2)

    while(True):
        try:
            promptOnErr = driver.find_element_by_xpath("//select[@id='MiscSettingsRef.ErrPrompt']")
            if promptOnErr.get_attribute('value') == 'string:Enabled':
                promptOnErr.send_keys(Keys.ARROW_DOWN)
            else:
                promptOnErr.send_keys(Keys.ARROW_UP)
        except Exception as e:
            print(e)
            continue
        break

    idracWeb.execute("//button[@translate='apply']")
    idracWeb.execute("//button[@ng-click='ok();']")
    idracWeb.execute("//button[@translate='apply_reboot']")
    idracWeb.execute("//button[@ng-click='onConfirmBtnAction(button.action)']")
    idracWeb.virtualConsoleLaunch()
    time.sleep(320)
    autoGuiActions.f2andclose()
    time.sleep(10)

